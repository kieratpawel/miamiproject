package com.miamiproject;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiamiProjectApplication {

	public static void main(String[] args) {
        //double[][] a = new double[2][3];
        //double[][] b = new double[3][2];

       // double[][] a= new double[][]{{2,1,3},{-1,4,0}};
       // double[][] b= new double[][]{{1,3,2},{-2,0,1},{5,-3,2}};

        double[][] a= new double[][]{{3,-4,5},{2,-3,1},{3,-5,-1}};

        double[][] b= new double[][]{{3,29},{2,18},{0,-3}};
        Matrix w = new Matrix(a);
        Matrix z = new Matrix(b);

        w.print();
        z.print();

        try {
            w.multiply(z).print();
            }
        catch(Exception e){
            System.out.println("das");
            }


        //SpringApplication.run(MiamiProjectApplication.class, args);
	}

}

