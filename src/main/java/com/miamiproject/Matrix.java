package com.miamiproject;

/**
 * Created by Znakomity on 2019-01-22.
 */
public class Matrix {

    private double[][] matrix;

    public Matrix(double[][] matrix)  {
        this.matrix=matrix;
    };


    public void print(){
        for (double[] a : this.matrix){
            for (double b : a){
                System.out.print(b + " ");

            }
            System.out.println();
    }
        System.out.println("+++++++++++++++++++++++");

    }
    public  Matrix multiply(Matrix a) throws Exception {

        if  (a.matrix.length != this.matrix[0].length){
            throw new Exception();
            }



        double[][] result = new double[this.matrix.length][a.matrix[0].length];

        for(int i=0;i<result.length;i++){

            for(int j=0;j<result[0].length;j++){

                for (int k = 0;k < a.matrix.length; k++){

                    result[i][j] +=a.matrix[k][j]*this.matrix[i][k];

                }

            }

        }
        return new Matrix(result);
    }
}
